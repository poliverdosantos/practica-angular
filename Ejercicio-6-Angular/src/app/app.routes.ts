import { RouterModule, Routes  } from "@angular/router";
import { InicioComponent } from "./components/inicio/inicio.component";
import { DocumentoComponent } from './components/documento/documento.component';
import { ContactoComponent } from './components/contacto/contacto.component';


const APP_ROUTES: Routes=[
    {path: 'inicio',component: InicioComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'inicio'}
]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);