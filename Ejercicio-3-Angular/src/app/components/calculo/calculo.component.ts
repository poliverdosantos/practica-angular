import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})  


export class CalculoComponent implements OnInit { 

   a:number=1;
   b:number=2;

  constructor() { }

  ngOnInit(): void {
  }

  sumar():number{
    return this.a + this.b;    
  }
  

}
